# Sample Evaluation Questions + Answers

There will only be 3 kinds of questions:

1. Write a program to do X.
2. Write the output of a given program.
3. Correct the errors in a given program.

### Programs and Outputs

-   String values: written within double quotes

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            System.out.println("Hello");
            System.out.println("1 + 5");
        }
    }
    ```

    ```out
    Hello
    1 + 5
    ```

-   Integer arithmetic

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            System.out.println(1 + 5);
            System.out.println(4 * 2);
            System.out.println(80 / 8);
        }
    }
    ```

    ```out
    6
    8
    10
    ```

-   Integer variable

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            int num = 16 - 4;
            System.out.println(num - 2);
        }
    }
    ```

    ```out
    10
    ```

-   String variable

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            String greet = "Hello there!";
            System.out.println(greet);
        }
    }
    ```

    ```out
    Hello there!
    ```

-   Division and modulus operators

    Note: When 72 is divided by 20, we get 3 as the quotient and 12 as the reminder.
    (i.e. 72 is 20 &times; 3 + 12)

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            System.out.println(72 / 20);
            System.out.println(72 % 20);
        }
    }
    ```

    ```out
    3
    12
    ```

-   Relational operators

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            System.out.println(10 < 8);
            System.out.println(15 > 8 + 1); // evaluates to 15 > 9
            System.out.println(12 % 2 == 0); // evaluates to 0 == 0
        }
    }
    ```

    ```out
    false
    true
    true
    ```

-   Branching 1

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            if (true) {
                System.out.println("Hello");
            } else {
                System.out.println("World");
            }
        }
    }
    ```

    ```out
    Hello
    ```

-   Branching 2

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            if (false) {
                System.out.println("Hello");
            } else {
                System.out.println("World");
            }
        }
    }
    ```

    ```out
    World
    ```

-   Branching 3

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            if (12 % 2 == 0) {
                System.out.println("12 is even");
            } else {
                System.out.println("12 is odd");
            }
        }
    }
    ```

    ```out
    12 is even
    ```

### Q&A

-   Write the output of the following program:

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            String x = "Hello Neo!";
            String y = "You are The One.";
            x = "Wanna hang out sometime?";
            System.out.println(y);
        }
    }
    ```

    **Answer:**

    ```out
    You are The One.
    ```

-   Spot the errors in the following program, and write the output after correction.

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            int hi = "12 + 4";
            System.out.println(hi);
        }
    }
    ```

    **Answer:**

    Type mismatched in the declaration statement. Possible corrections:

    ```java
        String hi = "12 + 4";
        System.out.println(hi);
    ```

    Output:

    ```out
    12 + 4
    ```

    or remove the quotes:

    ```java
        int hi = 12 + 4;
        System.out.println(hi);
    ```

    Output:

    ```out
    16
    ```

-   What would the below program output if the user enters `40` followed by `50`? 

    ```java
    import java.util.Scanner;
    public class Tutorial {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            System.out.println("Enter 2 numbers:");
            int a = s.nextInt();
            int b = s.nextInt();
            System.out.println("Here's their sum:");
            System.out.println(a + b);
        }
    }
    ```

    **Answer:**

    ```out
    Enter 2 numbers:
    40
    50
    Here's their sum:
    90
    ```

-   Write a program that takes a number as user input, multiplies it by `1024` and prints the result.

    **Answer:**

    ```java
    import java.util.Scanner;
    public class Tutorial {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            System.out.println("Enter a number:");
            int a = s.nextInt();
            System.out.println(a*1024);
        }
    }
    ```

-   Write the output of the following program:

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            System.out.println("Is it true that cats eat mouse?");
            System.out.println(12 < 0);
        }
    }
    ```

    **Answer:**

    ```out
    Is it true that cats eat mouse?
    false
    ```

-   Write a program that takes a number as input, and prints `Negative` if the number entered was less than zero, and prints `Positive` otherwise.

    **Answer:**

    ```java
    import java.util.Scanner;
    public class Tutorial {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            System.out.println("Enter a number:");
            int a = s.nextInt();
            if (a < 0) {
                System.out.println("Negative");
            } else {
                System.out.println("Positive");
            }
        }
    }
    ```

-   Correct the error in the following program. The corrected program should correctly print whether the number given as input is odd or even.

    ```java
    import java.util.Scanner;
    public class Test {
        public static void main(String[] args) {
            Scanner s = new Scanner(System.in);
            System.out.println("Enter some number:");
            int x = s.nextInt();
            if (x % 2) {
                System.out.println("It is even!");
            } else {
                System.out.println("It is odd!");
            }
        }
    }
    ```

    **Answer:**

    The condition given for `if` produces an integer value, and not a boolean value.

    I.e. `x % 2` produces an integer, and should be changed to `x % 2 == 0` so that it produces a boolean value such that `It is even!` will be printed when `x` is even. Thus the line becomes:

    ```java
            if (x % 2 == 0) {
    ```

### Practice questions

-   Write a program that prints the quotient and remainder when 123456 is divided by 789.
-   Write a program that prints `Yes!` if 1234567890 is greater than the product of 125000 and 9876, or `No!` otherwise.
-   Write a program that inputs 2 numbers and prints the quotient and reminder when the first number is divided by the second number.
-   Write a program that inputs 3 numbers and prints their sum as well as their product.
-   Write the output of the following program:

    ```java
    public class Test {
        public static void main(String[] args) {
            int x = 10;
            int y = -4;
            if (x > x + y) {
                System.out.println("Hello");
            }
            System.out.println("Bye");
        }
    }
    ```
