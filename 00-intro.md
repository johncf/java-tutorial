# Conceptual Introduction to Java

> **Basic Terminologies**
>
> A **program** may refer to something that can be readily run/executed (such as an application software), or it may refer to the "source code" that the application is generated from. **Source Code** refers to the human-readable form of a program that is written in a programming language such as Java.
>
> Source code written in Java needs to be converted into another form (called Java bytecode) in order to run (execute) the program. This is done by the **Java Compiler**, and this process of conversion is called **compilation**.
>
> Also note that "source code" is often shortened to just "code", although "code" often refers to smaller parts of the complete source code. So for our purposes, "program", "source code" and "code" might be used interchangeably.

## First Program

#### Example 1

```java
public class Tutorial {
    public static void main(String[] args) {
        System.out.println("Idiyappam!");
    }
}
```

This might be one of the simplest programs you can write in Java. Its basic structure is given below:

- The first line denotes the beginning of a **class definition** which every program needs to have.
- The second line denotes the start of the **`main` method** which every _executable_ program needs to have.
- The third line, which ends with a semi-colon `;`, is an example of a "statement".
- The fourth line denotes the end of the `main` method. Note that `}` there is closing the `{` from line two.
- Likewise, the last line denotes the end of the class definition. Note that `}` there matches `{` from line one.

Program execution starts from the beginning of the `main` method and stops when it reaches the end of it. In the above example, we only have one line inside `main`, which will "print" (displays onto screen or "outputs") the value inside parentheses. Thus, all our program does is just display:

```out
Idiyappam!
```

Let's add a similar line to our program:

#### Example 2

```java
public class Tutorial {
    public static void main(String[] args) {
        System.out.println("Idiyappam!");
        System.out.println("From the other side!!");
    }
}
```

This will produce an extra line of output:

```out
Idiyappam!
From the other side!!
```

A program is executed sequentially (sort of line-after-line). Notice that both lines end with a semi-colon `;`. A semi-colon denotes the end of a "statement," just like a full-stop denotes the end of a sentence in English.

> **What is a Statement?**
> 
> A **statement** is the smallest standalone element that expresses some action to be carried out.

#### Try it yourself (1)

-   Write a program that prints your name and your friend's name in different lines.

## Storing Values

One of the most essential feature all programming languages provide is a way to store values and retrieve them. This is done via something called **variables**.

#### Example 3

```java
public class Tutorial {
    public static void main(String[] args) {
        String s = "From the other side!!";
        System.out.println("Idiyappam!");
        System.out.println(s);
    }
}
```

The output is the same as in Example 2.

The first statement is an example of a **variable declaration**, which defines a variable named `s` and stores the text `From the other side!!` in it. In the final statement, we retrieve the value of `s` and prints it. `String` is the "type" of the variable `s` and it should match the "type" of the value being stored.

> **What is a Type?**
>
> **Types** are used to categorize and interpret different values.
>
> `String` is a type that represents text (i.e. sequence of letters/numerals/symbols). We'll discuss various other types available in Java later.

In the third statement, where the value stored in `s` is printed, note that we don't use double-quotes. Anything enclosed in double-quotes is treated as a `String` value. So `System.out.println("s");` would output the letter `s` and not the value stored in the variable `s`.

As we mentioned before, since the execution is sequential, the following program is *invalid*:

```java
public class Tutorial {
    public static void main(String[] args) {
        System.out.println("Idiyappam!");
        System.out.println(s);
        String s = "From the other side!!";
    }
}
```

The above program is invalid because the variable `s` is used *before* it is defined.

We can also change the value stored in a variable:

#### Example 4

```java
public class Tutorial {
    public static void main(String[] args) {
        String s = "Idiyappam!";
        System.out.println(s);
        s = "From the other side!!";
        System.out.println(s);
    }
}
```

The output is again the same as in Example 2.

The third statement that changes the value of `s` is an example of an **assignment statement**, which is different from the first statement which declares `s`. We must declare a variable exactly once before using it, and further changes to the variable are made using assignment statements.

Here's an example involving a numeric typed variable:

#### Example 5

```java
public class Tutorial {
    public static void main(String[] args) {
        int lucky = 8;
        System.out.println("Here's a number:");
        System.out.println(lucky);
        System.out.println("Let's multiply it by twelve:");
        lucky = lucky * 12;
        System.out.println(lucky);
        System.out.println("Ain't that great?!");
    }
}
```

Here's the output:

```out
Here's a number:
8
Can you multiply it by twelve?
96
Ain't that great?!
```

Here, we created a variable named `lucky` having type `int` to store an integer.

```java
lucky = lucky * 12
```

The above statement reads: "multiply the value of `lucky` with `12` and store (assign) the result to the variable `lucky`". Note that in an assignment statement, the expression on the right of `=` is evaluated first, and then the result is assigned to the variable on the left. Therefore, unlike mathematics, the order really matters. That is, the following statements are both *invalid*:

```java
60 = lucky;
lucky * 12 = lucky;
```

#### Try it yourself (2)

-   Write the output of the following program:

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            String x = "Hello Neo!";
            String y = "You are The One.";
            x = "Wanna hang out sometime?";
            System.out.println(y);
        }
    }
    ```

-   Are there any errors in the following program? Correct them, if any:

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            int hi = "1234";
            System.out.println(hi);
        }
    }
    ```

## User Interaction

User interaction can make programs a lot more interesting. Let's see how a basic interaction can be set up.

#### Example 6

```java
import java.util.Scanner;

public class Tutorial {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Enter 2 numbers:");
        int a = s.nextInt();
        int b = s.nextInt();
        System.out.println("Here's their sum:");
        System.out.println(a + b);
    }
}
```

That's a lot to take in at once. Let's break it down line-by-line:

```java
import java.util.Scanner;
```

The first line is called an **import statement**, which is used to bring extra features to our program. In this case, it tells the compiler that we are going to use a type called `Scanner` which is part of `java.util` package that is bundled with the Java compiler. `Scanner` provides a bunch of "methods" that can make it easier to work with user input.

```java
Scanner s = new Scanner(System.in);
```

This statement declares a variable named `s`, creates a "new `Scanner` object" that uses `System.in` underneath.

So what is `System.in`? Up till this point, notice how we were displaying outputs --- we were using the `println` "method" of `System.out`. In simple terms, `System.out` deals with program output, and `System.in` deals with input to the program.

Thus the above statement creates a new `Scanner` object which can read user input.

> #### What is a Method?
>
> A **method** is basically a piece of code that can be executed to perform a specific action. A **method call** is how we instruct the compiler to execute a method. When a method is called, it might take in one or more values (called **arguments**) and might produce a value (called **return value**).
>
> So far, we have seen `println` method of `System.out` object, and `nextInt` method of a `Scanner` object. `println` takes a single argument (the string to print given in parantheses) and returns nothing. On the other hand, `nextInt` takes no arguments and returns an integer value (the user input). If this sounds confusing right now, don't worry, we'll discuss this in more detail later.

```java
int a = s.nextInt();
int b = s.nextInt();
```

Both these statements are asking the `Scanner` object `s` to try to extract an integer value from its underlying "stream" (`System.in` --- user input). This will make the program to wait for user input, and if the user inputs an integer and presses enter, it will get stored in variable `a` and the program will wait again for user input, this time to be stored in variable `b`.

> **Sidenote:** The user input stream, `System.in`, is actually a stream of text, and if we provide non-numeric input when `nextInt` method of `Scanner` was called, the `Scanner` will throw an error and abort the program.

#### Try it yourself (3)

-   What would the above program output if the user enters `40` and then `50`? 
-   Write a program that takes a number as user input, multiplies it by `1024` and prints the result.

## Logic and Control Flow

Java has a type called `boolean` to work with logic. Here's an example to show you what that means:

#### Example 7

```java
public class Tutorial {
    public static void main(String[] args) {
        boolean isGreat = true;
        System.out.println("Are you doing great today?");
        System.out.println(isGreat);
        System.out.println("Is eight less than three?");
        System.out.println(8 < 3);
        System.out.println("Is five greater than four?");
        isGreat = 5 > 4;
        System.out.println(isGreat);
    }
}
```

The above program will produce the following output:

```out
Are you doing great today?
true
Is eight less than three?
false
Is five greater than four?
true
```

Let's break this program down.

```java
boolean isGreat = true;
```

Like we saw before, this is a declaration statement that creates a `boolean`-typed variable named `isGreat` and assigns a value to it. A `boolean` variable can take one of two values: `true` or `false`. In this case, variable `isGreat` is initialized with a boolean value `true`.

```java
System.out.println(8 < 3);
```

Here, the value obtained by evaluating the expression `8 < 3` will be printed. The less-than operator `<` takes two numeric operands and evaluates to a boolean value --- `true` if the left operand is less than the right one, or `false` otherwise. Thus, `8 < 3` evaluates to `false`.

```java
isGreat = 5 > 4;
```

Here, we are assigning the result of evaluating `5 > 4` to `isGreat`. Like before, the greater-than operator `>` also evaluates to a boolean value, and `5 > 4` evaluates to `true`.

#### Try it yourself (4)

-   Write the output of the following program:

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            System.out.println("Is it true that cats eat mouse?");
            System.out.println(12 < 0);
        }
    }
    ```

### Relational Operators

Here's a list of all relational operators in Java.

| Operator |           Name           |       Comments       |
|----------|--------------------------|----------------------|
| `>`      | Greater than             | `4 > 4` is `false`.  |
| `<`      | Less than                | `3 < 4` is `true`.   |
| `>=`     | Greater than or equal to | `4 >= 4` is `true`.  |
| `<=`     | Less than or equal to    | `3 <= 4` is `true`.  |
| `==`     | Equal to                 | `3 == 4` is `false`. |
| `!=`     | Not equal to             | `3 != 4` is `true`.  |

### Simple Branching

With the help of booleans, Java provides a mechanism to conditionally execute a bunch of statements.

#### Example 8

```java
public class Tutorial {
    public static void main(String[] args) {
        int marks = 80;
        if (marks > 60) {
            System.out.println("Wow, that's great!");
        } else {
            System.out.println("Better luck next time!");
        }
    }
}
```

The above program will generate the following output:

```out
Wow, that's great!
```

This is an example of an `if`-statement. To see how it works, let's start with the basic syntax:

```java
if (condition) {
    statement1;
} else {
    statement2;
}
```

Here, `condition` is a placeholder for an expression that *must* evaluate to a boolean value. If `condition` evaluates to `true`, everything within the first curly brackets gets executed (i.e. `statement1;`), otherwise if `condition` evaluates to `false`, everything within the curly brackets following `else` gets executed (i.e. `statement2;`).

So, back to our example, since `marks > 60` evaluates to `true`, only the first print-statement will be executed and the `else`-part will be skipped.

> **Sidenote:** This is called a "branching construct" because, if you look at the flow diagram below, you'll see that the program selects a "branch" based on a condition:
>
> ![if-then-else-flow](http://svgur.com/i/3T2.svg)

Also note that specifying the `else`-part is optional. So the following is also syntactically valid:

```java
if (condition) {
    statement1;
}
```

In this case, if the `condition` turned out to be `false`, program execution simply skips over whatever is in those curly brackets (i.e. `statement1;` will be skipped).

#### Try it yourself (5)

-   Write a program that asks the user to enter a number, and prints `Negative` if the number was less than zero, and prints `Positive` otherwise.
-   Write the output of the following program:

    ```java
    public class Tutorial {
        public static void main(String[] args) {
            boolean b = false;
            if (b) {
                System.out.println("Ninja");
            } else {
                System.out.println("Kitty");
            }
        }
    }
    ```

## Comments

When writing a program, it is often useful to include a summary of what a piece of code does along with the code itself. Such parts of source code which are completely ignored by the compiler is referred to as **comments**. Example 9 shows how comments are written in Java.

#### Example 9

```java
// this is a comment about this great class!
public class Tutorial {
    // here is another comment
    public static void main(String[] args) { // the compiler happily
        System.out.println("Idiyappam!"); // ignores all this crap!
    }
}
```

In the compiler's eyes, examples 1 and 9 are exactly the same.

## Numbers and Arithmetic

Now, let's do some math!

#### Example 10

```java
public class Tutorial {
    public static void main(String[] args) {
        int sum = 50 + 20;
        int diff = 50 - 20;
        int prod = 50 * 20;
        int quot = 50 / 20;
        int remd = 50 % 20;
        System.out.println(sum); // prints 70
        System.out.println(diff); // prints 30
        System.out.println(prod); // prints 1000
        System.out.println(quot); // prints 2
        System.out.println(remd); // prints 10
        System.out.println("Look ma! The computer does math!!");
    }
}
```

- The output of each line is mentioned in comments.
- The variable names stand for sum, difference, product, quotient and reminder respectively (in order from top).
- Percent-symbol `%` is called the **modulus operator** where `x % y` evaluates to the reminder when `x` is divided by `y`. So `50 % 20` will evaluate to `10` since 50 = 20 &times; 2 + 10.
- Also note, `50 / 20` evaluates to `2` (the quotient part in equation: Divident = Divisor &times; Quotient + Reminder). Remember that, `int`-type only allows integers.

### Floating Point Numbers

**Floating-point** system is an approximate representation of real numbers with limited precision (i.e. number of significant digits stored is limited).

#### Example 11

```java
public class Tutorial {
    public static void main(String[] args) {
        double quot = 50.0 / 20.0;
        System.out.println(quot); // prints 2.5
        System.out.println("Yeah!");
    }
}
```

Here, `double` stands for "double-precision floating-point numbers."

One important thing to note is that `50 / 20` is not the same as `50.0 / 20.0`, because in the former, an integer division is performed (resulting in `2`), while in the latter, a floating point division is performed. This is in turn due to `50` and `20` being given the type `int`, whereas `50.0` and `20.0` are given the type `double`.

#### Example 12

```java
public class Tutorial {
    public static void main(String[] args) {
        double quot = 50 / 20;
        System.out.println(quot); // prints 2.0
        System.out.println("Got it?");
    }
}
```

Even though the type of the variable `quot` is `double`, the evaluation of `50 / 20` is not affected by it, and consequently only an integral division is performed. The resulting integer `2` is converted to a `double` and stored in the variable `quot`. We'll discuss more about numeric representations and type conversions later.

#### Try it yourself (6)

- Write a program that takes a number from the user and checks whether it is divisible by 12. (Hint: Use modulus operator, and an `if`-statement)
- Write a program to calculate the length of the hypotenuse of a right triangle given the other two side lengths. (Hint: In Java, the square-root of 2 can be calculated as `Math.sqrt(2)`)

### Order of Evaluation

When you write a long expression with multiple operators and operands, the compiler uses the **operator precedence table** to figure out the order in which the operators must be applied. The entries in the table are sorted according to their precedence. Operators having higher precedence will appear above operators having lower precedence. Below is a partial version of Java's operator precedence table which only lists the operators that we have or will discuss in this tutorial.

| Precedence |         Operator         |                Name                | Associativity |
|------------|--------------------------|------------------------------------|---------------|
|          1 | `()`                     | Parantheses                        | --            |
|          1 | `.`                      | Member selection                   | left-to-right |
|          2 | `+` `-`                  | (Unary) Positive/negative operator | --            |
|          2 | `!`                      | (Unary) Logical NOT                | --            |
|          2 | `(type) value`           | (Unary) Explicit type cast         | --            |
|          3 | `/` `*`                  | Division/Multiplication operators  | left-to-right |
|          3 | `%`                      | Modulus operator                   | left-to-right |
|          4 | `+` `-`                  | Addition/Subtraction operators     | left-to-right |
|          5 | `<` `>` `<=` `>=`        | Relational less/greater operators  | left-to-right |
|          6 | `==` `!=`                | Relational equality operators      | left-to-right |
|          7 | `&&`                     | Logical AND                        | left-to-right |
|          8 | `||`                     | Logical OR                         | left-to-right |
|          9 | `=`                      | Simple assignment operator         | right-to-left |
|          9 | `+=` `-=` `*=` `/=` `%=` | Compound assignment operators      | right-to-left |

Let's take an example expression:

```java
x = 12 + 16 / 2 * 4;
```

Here, division and multiplication operators have the same precedence, and therefore `16 / 2 * 4` will be evaluated from left-to-right (i.e. `16 / 2` first followed by `8 * 4`). Next comes `+` (i.e. `12 + 32`), and finally the assignment (i.e. `x = 44`). Here are a few more examples:

```java
x = (12 + 16) / 2 * 4; // -->  28 / 2 * 4  -->  14 * 4  -->  56
x = 12 + 16 / (2 * 4); // -->  12 + 16 / 8  -->  12 + 2  -->  14
```

Many of the operators listed in the above table might be unfamiliar to you now. We'll explain them later.

## Conditional Repetitions

Sometimes we need to repeat a certain action multiple times, until some condition is met. This can be achieved in Java using the `while`-construct.

#### Example 13

```java
public class Tutorial {
    public static void main(String[] args) {
        System.out.println("Here are all natural numbers below 10:");
        int x = 1, sum = 0;
        while (x < 10) {
            System.out.println(x);
            sum = sum + x;
            x = x + 1;
        }
        System.out.println("Here's their sum:");
        System.out.println(sum);
    }
}
```

The above program will output:

```out
Here are all natural numbers below 10:
1
2
3
4
5
6
7
8
9
Here's their sum:
45
```

TODO explain

## Methods

TODO: Static methods
